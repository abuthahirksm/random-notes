import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';


import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ChatPage } from '../pages/chat/chat.component';

@Component({
    selector: 'menu-page',
    templateUrl: 'menu.html'
})
export class MenuPage {

    menu: any = { title: 'Home', name: 'Home', component: ChatPage };
    pages: Array<{ title: string, name: string, component: any }>;
    constructor(
        public navCtrl: NavController,
        private events: Events

    ) {
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home Screen', name: 'Home', component: HomePage },
            { title: 'List Screen', name: 'List', component: ListPage },
            { title: 'Chat Screen', name: 'Chat', component: ChatPage }

        ];
        //  this.menu = this.pages[0];
        let that = this;
        this.events.subscribe('menu:goto', (nav = 'Home') => {
            let pages = that.pages,
                p = (function () {
                    for (let i = 0; i < pages.length; i++) {
                        let page = pages[i];
                        if (page.name == nav) {
                            return page;
                        }
                    }
                    return pages[0];
                })();
            that.menu = p;

        });
    }

    gotoPage() {

    }

    setMenuPage(p) {
        if (p && p.component) {
            this.menu = p;
        }
    }
}
