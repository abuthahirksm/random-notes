import { Component } from '@angular/core';
import { NavController, NavParams , Events} from 'ionic-angular';
import { BusyService } from '../../shared/busy.service';
import { ListService } from '../list/list.service';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html',
  providers: [ListService]
})
export class ListPage {
  customerdata:any;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public busyService: BusyService,
    public listService: ListService,
    private events: Events
  ) {
    
  }

  ngOnInit(){
    ///this.getCustomerList();
    }

  getCustomerList(){


      let that = this;
       this.busyService.presentBusy('Please Wait...');
   this.listService.getCustomerList({})
       .then((res: any) => {
         that.busyService.dismissBusy();
           that.customerdata = res.data;
       });
  }

  gotoPage(nav = 'Home'){
    this.events.publish('menu:goto',nav);
  }
}
