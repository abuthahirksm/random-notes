/*
value: This is the plain text, chat message content;
type: Any of the following strings (plain_text, image, video, link, attachment, application, etc)
timeStamp: message generated time
data: any reference data or object
*/
export class ChatModal {
    value: String = '';
    type: String = 'plain_text';
    timeStamp: Date = new Date();
    data: any;
    scope: any;
    constructor(obj: any = {}) {
        let that = this,
            keys = Object.keys(obj),
            props = ['value', 'type', 'timeStamp'];
        try {
            for (let i = 0; i < keys.length; i++) {
                let key = keys[i],
                    oldType = typeof (that[key]),
                    newType = typeof (obj[key]);
                if (newType == oldType || oldType == 'undefined') {
                    that[key] = obj[key];
                } else {
                    let error = (key + ' is in ' + newType + '. expected type is ' + oldType);
                    throw error;
                }
            }
        }
        catch (e) {
            console.log(e);
        }
    }
}