import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { BusyService } from '../../shared/busy.service';
import { ChatService } from './chat.service';
import { ChatModal } from './chat.modal';
@Component({
    selector: 'page-chat',
    templateUrl: 'chat.component.html',
    providers: [ChatService]
})
export class ChatPage {
    chatData: any = [];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public busyService: BusyService,
        public chatService: ChatService,
        private events: Events
    ) {

    }

    ngOnInit() {
        for (let index = 0; index < 20; index++) {
            let sampleText = this.getSampleText(),
                chat = new ChatModal({
                    value: sampleText,
                    scope: this
                });
            this.chatData.push(chat);
        }
        console.log(this.chatData);
    }

    gotoPage(nav = 'Home') {
        this.events.publish('menu:goto', nav);
    }

    getSampleText() {
        let str = "",
            chars = "abcdefghijklmnopqrstuvwxyz   ",
            charelen = chars.length,
            l = 0,
            len = Math.round(Math.random() * 100);
        while (len > 0) {
            l = Math.round(Math.random() * charelen - 1);
            str += chars.substr(l, 1);
            len--;
        }
        return str;
    }
}
